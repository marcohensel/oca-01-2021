package basics;

public class SingletonTest {

	public static void main(String[] args) {
//		Singleton s1 = new Singleton(); // Compiler-Fehler, da Konstruktor private
//		Singleton s2 = new Singleton();
		Singleton s1 = Singleton.getInstance();
		Singleton s2 = Singleton.getInstance();
		Singleton s3 = Singleton.getInstance();
		System.out.println(s1 == s2);
		System.out.println(s1 == s3);
		System.out.println(s2 == s3);
	}

}

class Singleton {
	private static Singleton singleton = new Singleton();
	
	private Singleton() {
		
	}
	
	public static Singleton getInstance() {
		return singleton;
	}
	
}

class Singleton2 {
	private static Singleton2 singleton = null;
	
	private Singleton2() {
		
	}
	
	public static Singleton2 getInstance() {
		if (singleton == null) {
			singleton = new Singleton2();
		}
		return singleton;
	}
	
}