package zinsrechner;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;


public class ZinsrechnerView extends JFrame{

	private static final long serialVersionUID = 1L;
	// Deklaration der Komponenten
	// Bezeichnungsfelder (Label)
	JLabel lblStartkapital;
	JLabel lblZinssatz;
	JLabel lblLaufzeit;
	JLabel lblEndkapital;
	JLabel lblTitel;
	// Textfelder
	JTextField txfStartkapital;
	JTextField txfZinssatz;
	JTextField txfLaufzeit;
	JTextField txfEndkapital;
	// Textbereich
	JTextArea txaVerlauf;
	// ScrollPane
	JScrollPane scpVerlauf;
	// Befehls-Schaltf�chen
	JButton btnBerechnen;
	JButton btnBeenden;
	JButton btnLoeschen;
	// Optionsfelder (RadioButton)
	JRadioButton rbtEndkapital;
	JRadioButton rbtLaufzeit;
	// ButtonGroup zum Gruppieren der Optionsfelder
	ButtonGroup btgModus;
	// Container f�r die Komponenten (JPanel)
	JPanel pnlInhalt;
	JPanel pnlTitel;
	JPanel pnlGesamt;
	JPanel pnlButton;
	JPanel pnlVerlauf;
	// Men�leiste und Elemente deklarieren
	JMenuBar menuBar;
	JMenu mnuDatei;
	JMenu mnuInfo;
	JMenuItem mniBerechnen;
	JMenuItem mniLoeschen;
	JMenuItem mniBeenden;
	JMenuItem mniInfo;
	
	// Controller als Member-Variable
	ZinsrechnerController controller = new ZinsrechnerController(this);
	
	public ZinsrechnerView() {
		super("Zinsrechner"); // Titelleiste
//		this.setTitle("Zinsrechner"); // Alternative
		initComponents(); // Initialisierung der Componenten
//		Hinzuf�gen der Komponenten zu den Containern
		pnlTitel.add(lblTitel);
		pnlInhalt.add(lblStartkapital);
		pnlInhalt.add(txfStartkapital);
		pnlInhalt.add(lblZinssatz);
		pnlInhalt.add(txfZinssatz);
		pnlInhalt.add(lblLaufzeit);
		pnlInhalt.add(txfLaufzeit);
		pnlInhalt.add(lblEndkapital);
		pnlInhalt.add(txfEndkapital);
		pnlInhalt.add(rbtEndkapital);
		pnlInhalt.add(rbtLaufzeit);
		btgModus.add(rbtEndkapital);
		btgModus.add(rbtLaufzeit);
		pnlButton.add(btnBerechnen);
		pnlButton.add(btnLoeschen);
		pnlButton.add(btnBeenden);
		pnlVerlauf.add(scpVerlauf);
		pnlGesamt.add(pnlTitel);
		pnlGesamt.add(pnlInhalt);
		pnlGesamt.add(pnlButton);
		pnlGesamt.add(pnlVerlauf);
//		ActionListener f�r den Button Beenden ohne Model-View-Controller
//		btnBeenden.addActionListener(e -> System.exit(ABORT));
//		btnBeenden.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				System.exit(0);
//				
//			}
//		});
//		WindowListener f�r den JFrame
		this.addWindowListener(controller);
//		ActionListener f�r die Button und RadioButton mit Model-View-Controller
		btnBerechnen.addActionListener(controller);
		btnLoeschen.addActionListener(controller);
		btnBeenden.addActionListener(controller);
		rbtEndkapital.addActionListener(controller);
		rbtLaufzeit.addActionListener(controller);
		mniBerechnen.addActionListener(controller);
		mniLoeschen.addActionListener(controller);
		mniBeenden.addActionListener(controller);
		mniInfo.addActionListener(controller);
//		KeyListener f�r die Textfelder
		txfStartkapital.addKeyListener(controller);
		txfZinssatz.addKeyListener(controller);
		txfLaufzeit.addKeyListener(controller);
		txfEndkapital.addKeyListener(controller);
		
//		Layout des Panels setzen 
		pnlInhalt.setLayout(new GridLayout(5, 2, 30, 30));
		pnlGesamt.setLayout(new FlowLayout(FlowLayout.CENTER, 2000, 30));
		rbtEndkapital.setSelected(true); // Optionsfeld ist beim Start aktiviert
		txfEndkapital.setEditable(false); // Textfeld Endkapital nicht editierbar
		txaVerlauf.setFont(new Font("Courier New", Font.PLAIN, 12));
		scpVerlauf.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pnlGesamt.setBackground(Color.ORANGE); // Hintergrundfarbe des Inhaltsbereichs
		pnlTitel.setOpaque(false); // Panel durchsichtig machen
		pnlInhalt.setOpaque(false);
		pnlButton.setOpaque(false);
		pnlVerlauf.setOpaque(false);
		rbtEndkapital.setOpaque(false);
		rbtLaufzeit.setOpaque(false);
		
//		Men� bauen
		mnuDatei.setMnemonic('D');
		mnuInfo.setMnemonic('i');
		mniBerechnen.setMnemonic(KeyEvent.VK_B);
		mniLoeschen.setMnemonic(KeyEvent.VK_L);
		mniBeenden.setMnemonic(KeyEvent.VK_E);
		mniInfo.setMnemonic(KeyEvent.VK_I);
		mniInfo.setAccelerator(KeyStroke.getKeyStroke("control I"));
		menuBar.add(mnuDatei);
		menuBar.add(mnuInfo);
		mnuDatei.add(mniBerechnen);
		mnuDatei.add(mniLoeschen);
		mnuDatei.add(mniBeenden);
		mnuInfo.add(mniInfo);
		this.setJMenuBar(menuBar);
//		Setzen des Panels als Inhaltsbereich 
		this.setContentPane(pnlGesamt);
		this.setSize(350, 650); // Gr��e des Fensters in Breite mal H�he
		this.setLocationRelativeTo(null); // Fenster wird mittig angeordnet
//		this.setDefaultCloseOperation(EXIT_ON_CLOSE); // Anwendung wird beim Schlie�en des Fensters beendet
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Schlie�en des Fensters wird durch Listener abgedeckt
		this.setVisible(true); // Fesnster wird sichtbar gemacht
	}
	
	private void initComponents() {
		lblTitel = new JLabel("Zinsrechner");
		lblStartkapital = new JLabel("Startkapital");
		lblZinssatz = new JLabel("Zinssatz");
		lblLaufzeit = new JLabel("Laufzeit");
		lblEndkapital = new JLabel("Endkapital");
		
		txfStartkapital = new JTextField(10);
		txfZinssatz = new JTextField(10);
		txfLaufzeit = new JTextField(10);
		txfEndkapital = new JTextField(10);
		
		txaVerlauf = new JTextArea(10, 30);
		scpVerlauf = new JScrollPane(txaVerlauf);
		rbtEndkapital = new JRadioButton("Endkapital");
		rbtLaufzeit = new JRadioButton("Laufzeit");
		btgModus = new ButtonGroup();
		
		btnBerechnen = new JButton("Berechnen");
		btnBeenden = new JButton("Beenden");
		btnLoeschen = new JButton("L�schen");
		
		pnlInhalt = new JPanel();
		pnlTitel = new JPanel();
		pnlGesamt = new JPanel();
		pnlButton = new JPanel();
		pnlVerlauf = new JPanel();
		
		menuBar = new JMenuBar();
		mnuDatei = new JMenu("Datei");
		mnuInfo = new JMenu("Info");
		mniBerechnen = new JMenuItem("Berechnen");
		mniLoeschen = new JMenuItem("L�schen");
		mniBeenden = new JMenuItem("Beenden");
		mniInfo = new JMenuItem("Info", KeyEvent.VK_I);
	}

}
