package zinsrechner;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.NumberFormat;
import javax.swing.JOptionPane;

public class ZinsrechnerController extends WindowAdapter implements ActionListener, KeyListener{
	
	ZinsrechnerView view;
	ZinsrechnerModel model;
	
	public ZinsrechnerController(ZinsrechnerView view) {
		this.view = view;
		this.model = new ZinsrechnerModel();
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Beenden")) {
			beenden();
		} else if (e.getSource() == view.btnBerechnen || e.getSource() == view.mniBerechnen) {
			berechnen();
		} else if (e.getSource() == view.rbtEndkapital) {
			loeschen();
			view.txfLaufzeit.setEditable(true);
			view.txfEndkapital.setEditable(false);
		} else if (e.getSource() == view.rbtLaufzeit) {
			loeschen();
			view.txfLaufzeit.setEditable(false);
			view.txfEndkapital.setEditable(true);
		} else if (e.getActionCommand().equals("L�schen")) {
			loeschen();
		} else if (e.getActionCommand().equals("Info")) {
			info();
		}
	}

	private void info() {
		JOptionPane.showMessageDialog(view, "� 2021 Comcave" + System.lineSeparator() +  "Marco Hensel", "Info", JOptionPane.INFORMATION_MESSAGE);
		
	}

	private void berechnen() {
		// Inhalt der drei Textfelder auslesen (getText)
		String startkapitalString = view.txfStartkapital.getText();
		String zinssatzString = view.txfZinssatz.getText();
		String laufzeitString = null;
		String endkapitalString = null;
		// Umwandeln in die richtigen Datentypen
		int startkapital = Integer.parseInt(startkapitalString);
		double zinssatz = Double.parseDouble(zinssatzString);
		double laufzeit = 0;
		double endkapital = 0;

		if (view.rbtEndkapital.isSelected()) {
			laufzeitString = view.txfLaufzeit.getText();
			laufzeit = Integer.parseInt(laufzeitString);
			// Aufruf der berechneEndkapital-Methode aus dem Model
			endkapital = model.berechneEndkapital(startkapital, zinssatz, (int) laufzeit);
			// Ergebnis in Textfeld Endkapital eintragen (setText)
//			endkapitalString = String.format("%,.2f �", endkapital);
			endkapitalString = NumberFormat.getCurrencyInstance().format(endkapital);
			view.txfEndkapital.setText(endkapitalString);		

		} else if (view.rbtLaufzeit.isSelected()) {
			endkapitalString = view.txfEndkapital.getText();
			endkapital = Double.parseDouble(endkapitalString);
			// Aufruf der berechneLaufzeit-Methode aus dem Model
			laufzeit = model.berechneLaufzeit(startkapital, zinssatz, (int) endkapital);
			// Ergebnis in Textfeld Laufzeit eintragen (setText)
//			laufzeitString = String.format("%.1f", laufzeit);
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(1); // Anzahl der maximalen Nachkommastellen
			laufzeitString = nf.format(laufzeit);
			view.txfLaufzeit.setText(laufzeitString);
		}
//		view.txaVerlauf.setText("");
//		for (int jahr = 1; jahr <= laufzeit; jahr++) {
//			view.txaVerlauf.append(jahr + " " + NumberFormat.getCurrencyInstance().format(model.berechneEndkapital(startkapital, zinssatz, jahr)) + "\n");
//		}
		StringBuilder sbVerlauf = new StringBuilder();
		for (int jahr = 1; jahr <= laufzeit; jahr++) {
			sbVerlauf.append(String.format("%2d\t%,15.2f �%n", jahr, model.berechneEndkapital(startkapital, zinssatz, jahr)));
		}
		view.txaVerlauf.setText(sbVerlauf.toString());
		
	}

	private void loeschen() {
		// Inhalt der vier Textfelder und des Textbereichs l�schen
		view.txfStartkapital.setText("");
		view.txfZinssatz.setText("");
		view.txfLaufzeit.setText("");
		view.txfEndkapital.setText("");
		view.txaVerlauf.setText("");
		view.txfStartkapital.requestFocus(); // Cursor ins Texfeld Startkapital 
	}
	
	private void beenden() {
		int antwort = JOptionPane.showConfirmDialog(view, "Sind Sie sicher?", "Beenden", JOptionPane.YES_NO_OPTION);
		if (antwort == JOptionPane.YES_OPTION) {
			System.exit(0);
		}		
	}
	
	@Override
	public void windowClosing(WindowEvent e) {
		beenden();
	}

	@Override
	public void keyTyped(KeyEvent e) {
				
	}

	@Override
	public void keyPressed(KeyEvent e) {
							
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyChar() == KeyEvent.VK_ENTER) {
			if (e.getSource() == view.txfStartkapital) {
				view.txfZinssatz.requestFocus();
			} else if (e.getSource() == view.txfZinssatz) {
				if (view.rbtEndkapital.isSelected()) {
					view.txfLaufzeit.requestFocus();	
				} else if (view.rbtLaufzeit.isSelected()) {
					view.txfEndkapital.requestFocus();
				}
			} else if (e.getSource() == view.txfLaufzeit || e.getSource() == view.txfEndkapital) {
				berechnen();
			}
		}				
	}

	
}
