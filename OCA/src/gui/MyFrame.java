package gui;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame{ // Frame = Window + Titelleiste
	
	private static final long serialVersionUID = 1L;
	private JButton btnBeenden = new JButton("Beenden");
	private JLabel lblTitel = new JLabel("Mein erstes Fenster");
	
	public MyFrame(String titel) {
		super(titel); // Titelleiste
		this.setSize(400, 300); // Breite mal H�he
		this.setDefaultCloseOperation(EXIT_ON_CLOSE); // Was passiert beim Schlie�en des Frames?
		this.setLocationRelativeTo(null); // Positon des Frames mittig im Bildschirm
		this.getContentPane().setBackground(Color.BLUE);// Hintergrundfarbe der "Inhaltsscheibe" setzen
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(lblTitel);
		this.getContentPane().add(btnBeenden);
		
		btnBeenden.addActionListener((e) -> System.exit(0));
		
		this.setVisible(true); // sichtbar (default: nicht sichtbar)
	}

}
