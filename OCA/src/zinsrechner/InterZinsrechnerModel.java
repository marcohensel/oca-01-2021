package zinsrechner;

public interface InterZinsrechnerModel {
	
	double berechneEndkapital(int startkapital, double zinssatz, int laufzeit);

	double berechneLaufzeit(int startkapital, double zinssatz, int endkapital);
}
