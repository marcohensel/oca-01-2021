package plz;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class PLZFileModel implements PLZModel{
	
	private ArrayList<String> zeilen = new ArrayList<>();
	private String plzDatei = "files/plz.txt";
	
	 
	PLZFileModel() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(plzDatei), "UTF8")) ; // Dekoration des FilesReader mit einem BufferedReader => mehr komfortable Methoden, bessere Performance			
			String zeile;
			while ((zeile = br.readLine()) != null) {
				zeilen.add(zeile);			
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getOrt(String plz) {
		for (String zeile : zeilen) {
			if (zeile.startsWith(plz)) {
				return zeile.split("\t")[1];
			}
		}
		return null;
	}

	@Override
	public String getBundesland(String plz) {
		for (String zeile : zeilen) {
			if (zeile.startsWith(plz)) {
				return zeile.split("\t")[5];
			}
		}
		return null;
	}
	

}
