package plz;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PLZDBModel implements PLZModel{
	
	private String accessConnect = "jdbc:ucanaccess://files/PLZ_Datenbank.accdb";
	private Connection verbindung;
	private Statement befehl;
	
	PLZDBModel() {
		// Treiber laden (seit Java 1.7 nicht mehr zwingend)
//		try {
//			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
		try {
			verbindung = DriverManager.getConnection(accessConnect);
			System.out.println("Verbindung erfolgreich");
			befehl = verbindung.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public String getOrt(String plz) {
		boolean flag = false;
		String ret = null;
		try {
			ResultSet rs = befehl.executeQuery("select Ort from tblPLZ where PLZ = " + plz);	
			while (rs.next()) {
				if (flag) {
					ret += ", " + rs.getString("Ort");
				} else {
					flag = true;
					ret = rs.getString("Ort");
				}
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	public String getBundesland(String plz) {
		boolean flag = false;
		String ret = null;
		try {
			ResultSet rs = befehl.executeQuery("select Bundesland from tblPLZ where PLZ = " + plz);	
			while (rs.next()) {
				if (flag) {
					ret += ", " + rs.getString("Bundesland");
				} else {
					flag = true;
					ret = rs.getString("Bundesland");
				}
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

}
