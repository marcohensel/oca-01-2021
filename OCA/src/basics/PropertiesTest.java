package basics;

import java.util.Properties;

public class PropertiesTest {

	public static void main(String[] args) {
		Properties props = System.getProperties();
		props.list(System.out);
		String user = props.getProperty("user.name");
		System.out.println("User: " + user);
		// Manipulation der Java-Version zum Testen
		props.setProperty("java.version", "1.11.0_111");
		String version = props.getProperty("java.version");
		System.out.println("Java-Version: " + version);
		// �berpr�fung auf Java-Version mind. 1.7
		if (Double.parseDouble(version.substring(2,4)) < 8) {
			System.out.println("Sie m�ssen mindesten Java in der Version 1.8 installiert haben");		
		} else {
			System.out.println("Ihre Java-Version ist ausreichend.");
		}
		if (version.compareTo("1.8") < 0) {
			System.out.println("Sie m�ssen mindesten Java in der Version 1.8 installiert haben");		
		} else {
			System.out.println("Ihre Java-Version ist ausreichend.");
		}
		int versionInt = Integer.parseInt(version.split("\\.")[1]);
		versionInt = Integer.parseInt(version.substring(2,version.indexOf(".", 3)));
		System.out.println("Java-Version: " + versionInt);
	}

}
