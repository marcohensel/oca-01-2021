package plz;

public interface PLZModel {
	
	enum Modus{FILE, DB}
	
	String getOrt(String plz); // implizit public abstract
	
	String getBundesland(String plz);
	
	static PLZModel getInstance(Modus modus) {
		if (modus == Modus.FILE) {
			return new PLZFileModel();
		} else if (modus == Modus.DB) {
			return new PLZDBModel();
		}
		return null;
	}

}
