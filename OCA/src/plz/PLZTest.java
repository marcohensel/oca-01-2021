package plz;

public class PLZTest {

	public static void main(String[] args) {
		PLZModel model = PLZModel.getInstance(PLZModel.Modus.DB);
		// model.test();
		String plz = "03172";
		String ort = model.getOrt(plz);
		System.out.println("Ort: " + ort);
		String bundesland = model.getBundesland(plz);
		System.out.println("Bundesland: " + bundesland);

	}

}
