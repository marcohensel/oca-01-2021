package basics;

public class ArgsTest {

	public static void main(String... args) {
		if (args.length != 2) {
			throw new UnsupportedOperationException("Genau 2 Parameter eingeben");
		}
		for (int i = 0; i < args.length; i++) {
			System.out.println(i + ": " + args[i]);
		}
		int zahl1 = Integer.parseInt(args[0]);
		int zahl2 = Integer.parseInt(args[1]);
		System.out.println(zahl1 + zahl2);
		System.out.println("Servus");
	}

//  Compiler-Fehler: String[] und String...  sind identisch als Parameter
//	public static void doIt(String[] str) {
//		System.out.println("Array");
//	}
//	
//	public static void doIt(String... str) {
//		System.out.println("var_args");
//	}

}
